<?php
namespace App;

class Db
{
    private $server="localhost";
    private $user="";
    private $pass="";
    private $db="";
    private $mysqli;

    public function __construct($db="mdrsite"){
        $this->user="root";
        $this->db=$db;
        $this->mysqli = new \mysqli($this->server, $this->user, $this->pass, $this->db);

        if ($this->mysqli->connect_errno) {
            header("Location:404.php?msg=Error en la conexion a la base de datos");
        }
    }
    public function listarNoticias(){
        $resultado = $this->mysqli->query("SELECT * FROM noticia");
        if($resultado==false){
            header("Location:404.php?msg=Error en la lectura de la tabla NOTICIA");
        }
        return $resultado;
    }
}