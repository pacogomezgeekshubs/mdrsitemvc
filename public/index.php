<?php
    require_once dirname(__DIR__).'/vendor/autoload.php';
    use App\Db;

    $db=new Db();
    $resultado=$db->listarNoticias();
?>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <?php
            include './src/menu.php';
        ?>
        <div class="container">
            <?php
            $contador=1;
            foreach ($resultado as $fila) {
                if($contador==1) echo "<div class='row'>";
            ?>
                <div class="col-sm">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><?=$fila["titulo"]?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?=$fila["subtitulo"]?></h6>
                            <p class="card-text"><?=(substr($fila["noticia"],0,32)." ...")?></p>
                            <a href="noticia.php?id=<?=$fila["id"]?>" class="card-link">Leer mas</a>
                            <a class="card-link"><?=$fila["fecha"]?></a>
                        </div>
                    </div>
                </div>
            <?php  
                $contador++;
                if($contador>3){
                    $contador=1;
                    echo "</div>";
                }
            }
            ?>   
        </div>
    <!-- CARGA DE LIBERRIAS JS-->  
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>