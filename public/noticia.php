<?php
    $id=0;
    if(isset($_GET["id"]))$id=$_GET["id"];
    //Conexion a la base datos
    $mysqli = new mysqli("localhost", "root", "", "mdrsite");
    if ($mysqli->connect_errno) {
        header("Location:404.php?msg=Error en la conexion a la base de datos");
    }else{
        $resultado = $mysqli->query("SELECT * FROM noticia WHERE id=".$id);
        if($resultado==false){
            header("Location:404.php?msg=Error en la lectura de la tabla NOTICIA");
        }
    }
?>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <?php
            include './src/menu.php';
        ?>
        <?php
            foreach ($resultado as $fila) {
                echo $fila["titulo"];
            }
        ?>
    <!-- CARGA DE LIBERRIAS JS-->  
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>